-- Ingresar y usar una base de datos
USE jardineria;

SELECT * FROM cliente;
SELECT count(*) as 'Total de clientes' FROM cliente;
SELECT count(*) as 'Total de pagos' FROM pago;
SELECT count(*) as 'Total de detalle pedido' FROM detalle_pedido;
-- ¿Cuántos registros tiene cada tabla de
-- la base de datos Jardinería?

-- seleccionar todo de oficina
SELECT * FROM oficina;

-- Seleccionar todas las oficinas de España
SELECT * FROM oficina WHERE pais = 'España';
-- ¿Qué teléfonos son de las oficinas de España?
SELECT telefono FROM oficina WHERE pais = 'España';

-- Mostrar oficinas y teléfonos de España
SELECT concat('Oficina: ', codigo_oficina, ' ', ciudad, ', Teléfono: ', telefono) 
	as 'Oficinas con teléfono'
	FROM oficina 
    WHERE pais = 'España';
    
    
-- Nombre completo de los empleados que laboran en España
SELECT 
	concat(emp.nombre, ' ', emp.apellido1, ' ', emp.apellido2) as 'Nombre completo',
    ofi.pais as 'País'
	FROM empleado as emp
    INNER JOIN oficina as ofi
    ON emp.codigo_oficina = ofi.codigo_oficina
    WHERE pais = 'España';

-- Nombre completo de los empleados que laboran en España
SELECT 
	concat ('Hay ', count(emp.nombre), ' empleados españoles')
    as 'Empleados españoles'
	FROM empleado as emp
    INNER JOIN oficina as ofi
    ON emp.codigo_oficina = ofi.codigo_oficina
    WHERE pais = 'España';


